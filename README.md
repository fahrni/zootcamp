# README #

Little project to demonstrate a pragmatic approach to developing a Swift application that uses a service, displays a UITableView, and drops a pin in a map by looking up a lat/long based on a zip code. 

### What is a zootcamp? ###

Nothing, just a way to get the name pushed to the bottom of an alphbetized list. 

You should find to folders under the *zootcamp* root

* zootcamp - A Simple Swift iOS Application
* service - A little PHP code that acts as the service for *zootcamp*

### Swift Resources ###

If you have a Mac please grab Xcode, it's free to download from the Mac App Store.
* [Swift.org](https://swift.org/) - The open source repo for Swift.
* [Language Doc](https://swift.org/documentation/#the-swift-programming-language) - On the Swift.org site. 
* [Apple's Swift Page](https://developer.apple.com/swift/)
* [Apple Developer - Swift Resources](https://developer.apple.com/swift/resources/) - Sample code and Documentation
* [Apple Developer](https://developer.apple.com/) - Main Apple Developer Site. 


### Configuring a local PHP server ###

For now we're running on localhost. If you've never configured PHP to run locally you can check out some nice instructions [here](https://ole.michelsen.dk/blog/setup-local-web-server-apache-php-osx-yosemite.html).