//
//  Person.swift
//  zootcamp
//
//  Created by Rob Fahrni on 10/5/16.
//  Copyright © 2016 fahrni. All rights reserved.
//

import Foundation

class Person {
    // MARK: - Public Properties
    var firstName: String?
    var lastName: String?
    var age: Int?
    var zipCode: String?

    // MARK: - Lifecycle
    init(personJson: NSDictionary) {
        self.firstName = personJson["first_name"] as? String
        self.lastName = personJson["last_name"] as? String
        if let ageString = personJson["age"] as? String {
            self.age = Int(ageString)
        }
        self.zipCode = personJson["zip_code"] as? String
    }

    // MARK: - Class functions are static. They do not have a 'self' pointer
    class func buildPeopleFromData(data: NSData?) -> [Person]? {
        do {
            var people = [Person]()
            let peopleDict = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            guard let peopleArray = peopleDict.allValues as? [NSDictionary] else {
                return nil
            }
            for personDictionary in peopleArray {
                let person = Person(personJson: personDictionary)
                people.append(person)
            }
            return people
        } catch let error as NSError {
            print("ERROR = \(error)")
            return nil
        }
    }

}