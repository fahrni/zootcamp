//
//  MasterViewController.swift
//  zootcamp
//
//  Created by Rob Fahrni on 10/5/16.
//  Copyright © 2016 fahrni. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var people = [Person]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        // Kick the view to refresh(). 
        self.refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table View

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

        let person = people[indexPath.row]
        cell.textLabel!.text = person.firstName! + " " + person.lastName!
        
        return cell
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let person = people[indexPath.row]
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = person
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Private Methods

    private func refresh() {
        // Use Grand Central Dispatch to put the 'getPeople' call on a thread
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            ZootHttpClient.getPeople( { [weak self] (people: [Person]?, error: NSError?) -> Void in
                guard let weakself = self else {
                    return
                }
                weakself.people = people!

                // Reload the table on the main/UI thread
                dispatch_async(dispatch_get_main_queue()) {
                    weakself.tableView.reloadData()
                }
            })
        }
    }
}

