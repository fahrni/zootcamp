//
//  DetailViewController.swift
//  zootcamp
//
//  Created by Rob Fahrni on 10/5/16.
//  Copyright © 2016 fahrni. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    weak var mapView: MKMapView!

    var detailItem: Person? {
        didSet {
            // Update the view.
            self.updateView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.addMapView()
        self.updateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Private Methods

    private func addMapView() {
        let mapView = MKMapView(frame: self.view.bounds)
        mapView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.view.addSubview(mapView)
        self.mapView = mapView
    }

    private func updateView() {
        if let person = self.detailItem,
            let _ = mapView {
                dropPinFor(person.zipCode!)
        }
    }

    private func dropPinFor(zipCode: String) {
        assert(mapView != nil)
        mapView.removeAnnotations(mapView.annotations)

        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(zipCode, completionHandler: { [weak self] (placemarks, error) -> Void in
            if error != nil {
                print("Error: ", error)
            }
            guard let weakself = self else {
                return
            }
            if let placemark = placemarks?.first {
                weakself.mapView.addAnnotation(MKPlacemark(placemark: placemark))
                weakself.zoomMapToAnnotations()
            }
        })
    }

    private func zoomMapToAnnotations() {
        assert(mapView != nil)
        var zoomRect = MKMapRectNull
        for annotation in mapView.annotations {
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
            if MKMapRectIsNull(pointRect) {
                zoomRect = pointRect
            }
            else {
                zoomRect = MKMapRectUnion(zoomRect, pointRect)
            }

            mapView.setVisibleMapRect(zoomRect, animated: true)
        }
    }
}

