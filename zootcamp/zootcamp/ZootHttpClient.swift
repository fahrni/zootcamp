//
//  ZootHttpClient.swift
//  zootcamp
//
//  Created by Rob Fahrni on 10/5/16.
//  Copyright © 2016 fahrni. All rights reserved.
//
import Foundation

typealias PeopleCompletionHandler = ((people: [Person]?, error: NSError?) -> Void)

class ZootHttpClient {
    //static let PeopleUrl = "http://localhost/~fahrni/get-people.php"
    static let PeopleUrl = "http://mad.applecorelabs.com/zootcamp/get-people.php"

    class func getPeople(peopleCompletionHandler: PeopleCompletionHandler) {
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        guard let URL = NSURL(string: PeopleUrl) else {
            return
        }
        let request = NSMutableURLRequest(URL: URL)
        request.HTTPMethod = "GET"

        let task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            if (error == nil) {
                // Success
                guard let people = Person.buildPeopleFromData(data) else {
                    // Yeah, need an error here.
                    peopleCompletionHandler(people: nil, error: nil)
                    return
                }
                peopleCompletionHandler(people: people, error: nil)
            }
            else {
                // Failure
                peopleCompletionHandler(people: nil, error: error)
            }
        })

        task.resume()
        session.finishTasksAndInvalidate()
    }
}