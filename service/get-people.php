<?php
header("Content-Type: application/json");
header("Accept: application/json");

// Handles database connection (see connect.php)
include_once('connect.php');

$result_array = array();

$query = $conn->prepare("SELECT * FROM `people`"); 
$query->execute();
if ($query->rowCount() == 0) {
	// Switch this out to use standard HTTP errors.
	$result_array['error'] = 'error = No people found';
} else {
	// Iterate over the result set and build the array of people
	while ($fetch = $query->fetch()) {		
		$i++;

		$result_array[$i]['first_name'] = $fetch['first_name'];
		$result_array[$i]['last_name'] = $fetch['last_name'];
		$result_array[$i]['age'] = $fetch['age'];
		$result_array[$i]['zip_code'] = $fetch['zip_code'];
	}
}

// Return the array as JSON
echo json_encode($result_array);
?>